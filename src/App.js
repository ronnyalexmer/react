import React, { Component } from 'react';
import './App.css';

//componentes
import Navegacion from './components/navegacion/navegacion';
import Card from './components/card/card';

//recursos
import { week } from './components/datos/week.json';
import * as utils from './utils/utils';


class App extends Component {
  constructor(){
    super();
    this.state = {
      titulo: "Tablón hecho en React",
      week:  week
    }
  }

  render() {
    const cards = this.state.week.map((dayObj, i)=>{
      return(<Card day={dayObj.day} date={utils.daysPlus(i)} work={dayObj.work} gym={dayObj.gym} />);
    });

    return (
      <div className="App">
        <Navegacion titulo={this.state.titulo} />
        <div className="container-fluid">
          <div className="row mt-4">
            {cards}
          </div>
        </div>
      </div>
      );
    }
  }

  export default App;
