import React, { Component } from 'react';
import logo from '../../logo.svg';
import './navegacion.css';

class Navegacion extends Component {
  render() {
    return (
	  <nav className="navbar navbar-light bg-light">
	    <a className="navbar-brand" href="#">
	      <img src={logo} width="30" height="30" className="d-inline-block align-top logo" alt="" />
	      {this.props.titulo}
	    </a>
	  </nav>
    );
  }
}

export default Navegacion;