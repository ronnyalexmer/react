import React, { Component } from 'react';
import './card.css';

class Card extends Component{
	render(){
		return(
			<div className="col-md-4 mt-4">
				<div className="card">
				  <div className="card-body">
				    <h5 className="card-title">{this.props.day}</h5>
				    <h6 className="card-subtitle mb-2 text-muted">{this.props.date}</h6>
				    <p className="card-text">{this.props.work}</p>
				    <p className="card-text">{this.props.gym}</p>
				  </div>
				</div>
			</div>
		);
	}
}
export default Card;