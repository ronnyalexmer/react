const today = new Date();

export function formatDate(date){
  var dd = date.getDate();
  var mm = date.getMonth()+1; 
  var yyyy = date.getFullYear();

  dd = dd<10 ? "0"+dd : dd;
  mm = mm<10 ? "0"+mm : mm;

  return dd+"/"+mm+"/"+yyyy;
};

export function daysPlus(day){
  var nextDay = new Date();
  nextDay.setDate(today.getDate()+day);
  return formatDate(nextDay);
}
